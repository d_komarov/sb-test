import * as React from 'react';
import styled from 'styled-components';

const Ul = styled.ul`
  width: 100%;
  padding: 0;
  padding-top: 20px;
  margin: 0;
  list-style: none;

  & & {
    display: none;
    position: absolute;
    z-index: 3;
    width: 100%;
    height: 100%;
    background: #000;
    top: 0;
    left: 100%;
  }
`;

const NameLi = styled.li`
  box-sizing: border-box;
  width: 100%;
  display: block;
  padding: 15px 20px;
  color: #fff;
  border-bottom: 1px solid rgba(255,255,255,0.3);
`;

const menu: React.SFC<{name?: string}> = ({ name, children }) => {

  if (name) {
    return (
      <React.Fragment>
        <Ul>
          <NameLi>{name}</NameLi>
          {children}
        </Ul>
      </React.Fragment>
    );
  }

  return <Ul>{children}</Ul>;

};

export default menu;
