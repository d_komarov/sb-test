import * as React from 'react';
import styled, { css, keyframes } from 'styled-components';

interface Props {
  isOpen: boolean;
  onSetOpen: (isOpen: boolean) => void;
  className?: string;
}

interface State {
  opening: boolean;
}

const SidebarRoot = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  overflow: hidden;
`;

const SidebarContent = styled.div`
  overflow-y: auto;
  max-height: 100%;
  min-height: 100%;
`;

const SidebarMainAnimationOpen = keyframes`
  from {
    transform: translate(-100%, 0);
  }

  to {
    transform: translate(0, 0);
  }
`;

const SidebarMainAnimationClose = keyframes`
  from {
    transform: translate(0, 0);
  }

  to {
    transform: translate(-100%, 0);
  }
`;

const SidebarMain = styled.div<State>`
  z-index: 2;
  position: absolute;
  top: 0;
  bottom: 0;
  width: 300px;
  overflow: visible;
  background: #1E232B;
  ${props => props.opening
    ? css` animation: ${SidebarMainAnimationOpen} 0.3s 1;`
    : css` animation: ${SidebarMainAnimationClose} 0.3s 1;`
  }
`;

const SidebarOverlay = styled.div`
  z-index: 1;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(0,0,0,.3);
`;

export default class Sidebar extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      opening: props.isOpen,
    };
  }

  overlayClicked = () => {
    this.setState({opening: false});
    setTimeout(
      () => {
        this.props.onSetOpen(false);
      },
      300,
    );
  }

  componentDidUpdate(prev: Props) {
    if (this.props.isOpen && !prev.isOpen) {
      this.setState({opening: true});
    }
  }

  render() {
    const { className, isOpen, children } = this.props;
    const { opening } = this.state;
    if (!isOpen) {
      return false;
    }

    return (
      <SidebarRoot className={className}>
        <SidebarMain opening={opening} >
          <SidebarContent>
            {children}
          </SidebarContent>
        </SidebarMain>
        <SidebarOverlay onClick={this.overlayClicked} />
      </SidebarRoot>
    );
  }
}
