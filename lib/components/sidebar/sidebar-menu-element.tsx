import * as React from 'react';
import styled from 'styled-components';

const Li = styled.li`
  width: 100%;
  padding: 0;
  border-bottom: 1px solid rgba(255,255,255,0.3);
  &:hover ul {
    display: block;
  }
  &:hover {
      background: #000;
  }
  &:hover ul a:hover {
    background: #1E232B;
  }

  &:hover ul li {
  border-bottom: 1px solid rgba(255,255,255,0.0);
  }
  a {
    box-sizing: border-box;
    width: 100%;
    display: block;
    padding: 15px 20px;
    color: #fff;
    text-decoration: none;
    position: relative;
  }

  &.hasChildren > a:after {
    content: '>';
    display: block;
    position: absolute;
    right: 15px;
    top: 16px;
  }

  &.hasChildren:hover > a:after {
    content: '<';
  }
`;

const element: React.SFC = ({ children }) => {
  const count = React.Children.count(children);
  return (
    <Li className={count > 1 ? 'hasChildren' : ''}>
      {children}
    </Li>
  );
};

export default element;
