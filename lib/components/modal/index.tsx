import * as React from 'react';
import * as RD from 'react-dom';
import styled from 'styled-components';

const Overlay = styled.div`
  z-index: 1;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(0,0,0,.3);
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ModalContent = styled.div`
  width: 400px;
  min-height: 200px;
  padding: 20px;
  background: #1B1F26;
`;

interface Props {
  modalRoot: string;
  isOpen: boolean;
  onClose: () => void;
}

export default class Modal extends React.Component<Props> {
  modalRoot: HTMLElement | null;
  ovelay: any;

  handleOverlayClick = (event: any) => {
    if (event.target === this.ovelay) {
      this.props.onClose();
    }
  }

  render() {
    this.modalRoot = document.getElementById(this.props.modalRoot);

    if (!this.props.isOpen || !this.modalRoot) {
      return false;
    }

    return this.props.isOpen && this.modalRoot && RD.createPortal(
      (
        <Overlay ref={ref => this.ovelay = ref} onClick={this.handleOverlayClick}>
          <ModalContent>
            {this.props.children}
          </ModalContent>
        </Overlay>
      ),
      this.modalRoot,
    );
  }
}
