import * as React from 'react';
import styled from 'styled-components';

interface Props {
  checked: boolean;
  onChange: (checked: boolean) => void;
}

const CheckboxWrapper = styled.div`
  text-align: center;
  input {
    display: none;
  }

  input + .table-checkbox {
    border: 1px solid rgba(255,255,255,0.2);
    display: inline-block;
    width: 18px;
    height: 18px;
    border-radius: 100%;
    position: relative;
  }

  input:checked + .table-checkbox {
    background: #1330FF;
  }
  input:checked + .table-checkbox:before {
    content: "✔";
    font-size: 14px;
    line-height: 18px;
    position: absolute;
    left: 4px;
  }
`;

export default class TableCheckBox extends React.Component<Props> {

  handleCheck = () => {
    this.props.onChange(!this.props.checked);
  }

  render () {
    return (
      <CheckboxWrapper>
        <label>
          <input type="checkbox" onChange={this.handleCheck} checked={this.props.checked}/>
          <span className={'table-checkbox'}></span>
        </label>
      </CheckboxWrapper>
    );
  }
}
