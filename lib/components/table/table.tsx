import * as React from 'react';
import styled, { keyframes } from 'styled-components';

import Checkbox from './table-checkbox';

export interface Column {
  name: string;
  property: string;
  onSort?: (columnName: string, sortDirection: 'asc'|'desc') => void;
  cellComponent?: React.SFC<{cellData: any}>;
}

interface Props {
  tableName: string;
  data: any[];
  idProperty: string;
  columns: Column[];
  loading: boolean;
  groupActions?: React.SFC<{ids: string[] | number[]}>[];
  checkboxs?: boolean;
  expandable?: boolean;
  onSelect?: (selectedIds: string[] | number[]) => void;
  onEndTable?: () => void;
}

interface State {
  loadRequested: boolean;
  selectedIds: string[] | number[];
  expandedRow: string | number | null;
  checkedAll: boolean;
  groupActionsIsOpen: boolean;
}

const TableTag = styled.table`
  width: 100%;
  color: #fff;
  border:none;
  border-collapse:collapse;
  border-spacing:0;
`;

const Tr = styled.tr`
  background: #181B20;
  &:nth-child(2n) {
    background: #1B1F25;
  }
`;

const expandAnimation = keyframes`
  0% {
    max-height: 0;
  }

  100% {
    max-height: 15em;
  }
`;

const Td = styled.td`
  text-align: left;
  padding: 20px 0;
  .td-inner {
    padding-left: 10px;
    border-left: 1px solid rgba(255,255,255,0.2);
  }

  .expanded-content {
    padding: 0 20px;
    text-align: center;
    box-sizing: border-box;
    animation: ${expandAnimation} .5s 1;
    overflow: hidden;
  }

  &.expand-button {
    text-align: center;
  }
`;

const Th = styled.th`
  text-align: left;
  padding: 20px 10px;
`;

const Thead = styled.thead`
  tr {
    background: #1B1F25;
  }
`;

const TableRoot = styled.div`
  background: #15171B;
  padding: 0 20px;
  text-align: right;
`;

const loaderAnimation = keyframes`
  0% {
    transform: rotate(0);
    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }
  50% {
    transform: rotate(900deg);
    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
  }
  100% {
    transform: rotate(1800deg);
  }
`;

const Loader = styled.div`
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
  &:after {
    content: " ";
    display: block;
    border-radius: 50%;
    width: 0;
    height: 0;
    margin: 6px;
    box-sizing: border-box;
    border: 26px solid #fff;
    border-color: #fff transparent #fff transparent;
    animation: ${loaderAnimation} 1.2s infinite;
  }
`;

const ExpandButton = styled.button`
  padding: 0;
  border: none;
  font-size: 0;
  background: transparent;
  &:before {
    content: "↓";
    font-size: 18px;
    color: #fff;
  }

  &.expanded:before  {
    content: "↑";
  }
`;

const GroupActions = styled.div`
  position: relative;
  display: inline-block;
  padding: 20px 0;
`;

const GroupActionsButton = styled.button`
  background: transparent;
  border: none;
  color: #fff;
`;

const GroupActionsList = styled.ul`
  position: absolute;
  box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.75);
  background: #1B1F25;
  width: 200px;
  top: 46px;
  right: 0;
  margin: 0;
  padding: 0 10px;
  text-align: left;
  li {
    list-style: none;
    padding: 10px 0;
  }
`;

export default class Table extends React.Component<Props, State> {
  tfoot: any;
  groupActionsListNode: any;
  state = {
    loadRequested: true,
    selectedIds: [] as string[] | number[],
    expandedRow: null,
    checkedAll: false,
    groupActionsIsOpen: false,
  };

  componentDidMount() {
    document.addEventListener('scroll', this.isInViewport, true);
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.isInViewport, true);
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.data !== this.props.data) {
      this.setState(
        {
          loadRequested: false,
          checkedAll: this.state.selectedIds.length === this.props.data.length,
        },
        () => this.isInViewport(),
      );
    }
  }

  renderTable(tableData: any[]) {
    const rows = tableData.map(rowData => this.renderRow(rowData));

    return (
      <TableTag>
        {this.renderHeader()}
        <tbody>{rows}</tbody>
        {this.renderLoader()}
      </TableTag>
    );
  }

  renderHeader() {
    const { columns, checkboxs, expandable } = this.props;
    const cells = columns.map(column => <Th key={column.name}>{column.name}</Th>);

    if (checkboxs) {
      cells.unshift((
        <Th  key={'checkbox'}>
          <Checkbox
            checked={this.state.checkedAll}
            onChange={this.handleSelectAll}
          />
        </Th>
      ));
    }
    if (expandable) {
      cells.unshift(<Th  key={'expandable'}/>);
    }
    return (
      <Thead>
        <Tr>
          {cells}
        </Tr>
      </Thead>
    );

  }

  renderRow(rowData: any) {
    const { columns, idProperty, checkboxs, expandable } = this.props;
    const { expandedRow, selectedIds } = this.state;
    const id: string = rowData[idProperty];

    const cells = columns.map(column => this.renderCell(
      rowData[column.property],
      column,
      `${id}_${column.property}`,
    ));
    if (checkboxs) {
      cells.unshift((
        <Td  key={'checkbox'}>
          <Checkbox
            checked={(selectedIds as string[]).includes(id)}
            onChange={this.handleSelect(id)}
          />
        </Td>
      ));
    }

    if (expandable) {
      cells.unshift((
        <Td  className="expand-button" key={'expandable'}>
          <ExpandButton
            className={expandedRow === id ? 'expanded' : ''}
            onClick={this.handleExpand(id)} >
            expand
          </ExpandButton>
        </Td>
      ));
    }

    if (expandedRow === id) {
      return [
        (<Tr className="expanded" key={id}>{cells}</Tr>),
        (
          <Tr key={`${id}_expanded`}>
            <Td  colSpan={columns.length + 1 + (checkboxs ? 1 : 0)}>
              <div className="expanded-content">
                <img
                  width="728px"
                  height="90px"
                  src={`https://via.placeholder.com/728x90.png?text=Expanded+row+${id}`}
                  alt=""
                />
              </div>
            </Td>
          </Tr>
        ),
      ];

    }

    return <Tr key={id}>{cells}</Tr>;
  }

  renderCell(cellData: any, column: Column, id: any) {
    const CellComponent = column.cellComponent;
    if (CellComponent) {
      return <CellComponent key={id} cellData={cellData}/>;
    }
    return <Td key={id}><span className="td-inner">{cellData}</span></Td>;
  }

  renderLoader() {
    const { loading, columns, checkboxs, expandable } = this.props;

    return (
      <tfoot ref={ref => this.tfoot = ref}>
        <Tr>
          <Td colSpan={columns.length + (checkboxs ? 1 : 0) + (expandable ? 1 : 0)}>
            {loading && <Loader/>}
          </Td>
        </Tr>
      </tfoot>
    );
  }

  renderGroupActions() {
    const { groupActions } = this.props;
    const { groupActionsIsOpen } = this.state;
    if (!groupActions) {
      return false;
    }

    return (
      <GroupActions>
        <GroupActionsButton
          onClick={this.handleGroupActionsButtonClick}
        >
          Group Actions
        </GroupActionsButton>
        {
          groupActionsIsOpen && (
            <GroupActionsList ref={node =>  this.groupActionsListNode = node}>
              {this.renderGroupActionsList()}
            </GroupActionsList>
          )
        }
      </GroupActions>
    );
  }

  renderGroupActionsList() {
    const { groupActions } = this.props;
    const { selectedIds } = this.state;
    if (!groupActions) {
      return false;
    }

    return groupActions.map(Ga => (
      <li key={Ga.name}>
        <Ga ids={selectedIds}/>
      </li>
    ));
  }

  isInViewport = () => {
    const offset = 0;
    const { loadRequested } = this.state;
    const { onEndTable, loading } = this.props;
    if (loading || !onEndTable || loadRequested || !this.tfoot) return;
    const top = this.tfoot.getBoundingClientRect().top;
    if ((top + offset) >= 0 && (top - offset) <= window.innerHeight) {
      onEndTable();
      this.setState({ loadRequested: true });
    }
  }

  handleGroupActionsButtonClick = () => {
    const { groupActionsIsOpen } = this.state;

    if (!groupActionsIsOpen) {
      document.addEventListener('click', this.handleOutsideClick, false);
    } else {
      document.removeEventListener('click', this.handleOutsideClick, false);
    }

    this.setState({
      groupActionsIsOpen: !groupActionsIsOpen,
    });
  }

  handleOutsideClick = (e: Event) => {
    if (this.groupActionsListNode.contains(e.target)) {
      return;
    }

    this.handleGroupActionsButtonClick();
  }

  handleSelect = (id: string | number) => (checked: boolean) => {
    let selectedIds: string[] | number[] = this.state.selectedIds;
    const { onSelect, data } = this.props;

    if (!onSelect) {
      return;
    }

    if (!checked) {
      selectedIds = (selectedIds as string[]).filter(selectedId => selectedId !== id);
    } else {
      selectedIds = [
        ...selectedIds,
        id,
      ] as string[] | number[];
    }

    this.setState({ selectedIds, checkedAll: selectedIds.length === data.length });
    onSelect(selectedIds);
  }

  handleSelectAll = (checkedAll: boolean) => {
    const { onSelect, data, idProperty } = this.props;
    if (!onSelect) {
      return;
    }

    let selectedIds: string[] | number[] = [];
    if (checkedAll) {
      selectedIds = data.map(row => row[idProperty]);
    }

    this.setState({ checkedAll, selectedIds });
    onSelect(selectedIds);
  }

  handleExpand = (id: string | number) => () => {
    const { expandedRow } = this.state;

    this.setState({
      expandedRow: id === expandedRow ? null : id,
    });
  }

  render() {
    const { data } = this.props;
    return (
      <TableRoot>
        {this.renderGroupActions()}
        {this.renderTable(data)}
      </TableRoot>
    );
  }
}
