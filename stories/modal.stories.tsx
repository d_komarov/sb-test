import * as React from 'react';
import styled from 'styled-components';

import { storiesOf } from '@storybook/react';
// import { action } from '@storybook/addon-actions';

import { Modal } from '../lib';

const ModalContent = styled.div`
  text-align: center;
  color: #fff;
`;

class ModalWrapper extends React.Component<{}, {modalIsOpen: boolean}> {

  state = {
    modalIsOpen: false,
  };

  handleShowModalClick = () => {
    this.setState({ modalIsOpen: true });
  }

  handleModalClose = () => {
    this.setState({ modalIsOpen: false });
  }

  render() {

    return (
      <React.Fragment>
        <button onClick={this.handleShowModalClick}>Show Modal</button>
        <Modal
          isOpen={this.state.modalIsOpen}
          modalRoot="root"
          onClose={this.handleModalClose}
        >
          <ModalContent>
            Modal content
          </ModalContent>
        </Modal>
      </React.Fragment>
    );
  }
}

storiesOf('Modal', module)
  .add('Modal', () => (
    <ModalWrapper/>
  ));
