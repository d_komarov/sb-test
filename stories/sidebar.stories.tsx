import * as React from 'react';

import { storiesOf } from '@storybook/react';
// import { action } from '@storybook/addon-actions';

import { Sidebar, SidebarMenu, SidebarMenuElement } from '../lib';

class SidebarWrapper extends React.Component {

  state = {
    sidebarOpen: false,
  };

  onSetSidebarOpen = (isOpen: boolean) => {
    this.setState({ sidebarOpen: isOpen });
  }

  render() {
    return (
      <React.Fragment>
        <button onClick={() => this.onSetSidebarOpen(true)}>
          Open sidebar
        </button>
      <Sidebar
        isOpen={this.state.sidebarOpen}
        onSetOpen={this.onSetSidebarOpen}
      >
        <SidebarMenu>
          <SidebarMenuElement>
            <a href="#">test</a>
          </SidebarMenuElement>
          <SidebarMenuElement>
            <a href="#">test2</a>
          </SidebarMenuElement>
          <SidebarMenuElement>
            <a href="#">test3</a>
            <SidebarMenu name={'test3'}>
              <SidebarMenuElement>
                <a href="#">test4</a>
              </SidebarMenuElement>
              <SidebarMenuElement>
                <a href="#">test5</a>
              </SidebarMenuElement>
            </SidebarMenu>
          </SidebarMenuElement>
          <SidebarMenuElement>
            <a href="#">test6</a>
            <SidebarMenu name={'test6'}>
              <SidebarMenuElement>
                <a href="#">test7</a>
              </SidebarMenuElement>
              <SidebarMenuElement>
                <a href="#">test8</a>
              </SidebarMenuElement>
            </SidebarMenu>
          </SidebarMenuElement>
        </SidebarMenu>
      </Sidebar>

      </React.Fragment>
    );
  }
}

storiesOf('Sidebar', module)
  .add('Sidebar', () => (
    <SidebarWrapper   />
  ));
