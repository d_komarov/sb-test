import * as React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Table } from '../lib';

const columns = [
  {
    name: 'Name',
    property: 'name',
  },
  {
    name: 'Year',
    property: 'year',
  },
  {
    name: 'Color',
    property: 'color',
  },
  {
    name: 'Pantone value',
    property: 'pantone_value',
  },
];

const groupAction: React.SFC<{ids: string[] | number[]}> = ({ ids }) => {
  return (
    <button onClick={() => action('group action ids')(ids.join(' '))}>
      group action example
    </button>
  );
};

interface State {
  data: any[];
  loading: boolean;
  page: number;
  allLoaded: boolean;
}

class TableWrapper extends React.Component<{}, State> {

  state = {
    data: [],
    loading: true,
    allLoaded: false,
    page: 1,
  };

  componentDidMount() {
    this.loadData();
  }

  loadData = () => {
    const { page, data, allLoaded } = this.state;
    if (allLoaded) {
      return;
    }
    this.setState({ loading: true });
    fetch(`https://reqres.in/api/unknown?page=${page}&per_page=3&delay=1`)
      .then(res => res.json())
      .then((json) => {
        if (json.data.length) {
          this.setState({
            data: [...data, ...json.data],
            loading: false,
            page: page + 1,
            allLoaded: json.page === json.total_pages,
          });
        } else {
          this.setState({
            loading: false,
          });
        }
      });
  }

  render() {
    return (
      <Table
        tableName="test Table"
        data={this.state.data}
        idProperty="id"
        loading={this.state.loading}
        columns={columns}
        onEndTable={this.loadData}
        checkboxs={true}
        onSelect={action('onSelect')}
        expandable={true}
        groupActions={[groupAction]}
      />
    );
  }
}

storiesOf('Table', module)
  .add('Table', () => (
    <TableWrapper   />
  ));
